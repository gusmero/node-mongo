FROM node:16.13.1-alpine
COPY . .
RUN npm install
EXPOSE 8443
CMD ["node" "app.js"]

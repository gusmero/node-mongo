const { MongoClient } = require('mongodb');
// or as an es module:
// import { MongoClient } from 'mongodb'

// Connection URL local
const url = 'mongodb://localhost:27017';
//connection URL docker-compose
//const url = 'mongodb://mongodb:27017';
//const url = 'mongodb://root:password@mongodb:27017';

const client = new MongoClient(url);

// Database Name
const dbName = 'myProject';

async function main() {
  // Use connect method to connect to the server
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db(dbName);
  const collection = db.collection('documents');

  // insert a document
  const insertResult = await collection.insertMany([{ a: 1 }, { a: 2 }, { a: 3 }]);
  console.log('Inserted documents =>', insertResult);

  //find all documents
  const findResult = await collection.find({}).toArray();
  console.log('Found documents =>', findResult);

  //find Documents with a query filter
  const filteredDocs = await collection.find({ a: 3 }).toArray();
  console.log('Found documents filtered by { a: 3 } =>', filteredDocs); 

  //Update a document
  const updateResult = await collection.updateOne({ a: 3 }, { $set: { b: 1 } });
  console.log('Updated documents =>', updateResult);

  //Remove a document
  const deleteResult = await collection.deleteMany({ a: 3 });
  console.log('Deleted documents =>', deleteResult);


  //index a collection
  const indexName = await collection.createIndex({ a: 1 });
  console.log('index name =', indexName);

  return 'done.';
}

main()
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close());
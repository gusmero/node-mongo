var express = require('express');
const { MongoClient } = require('mongodb');

// Connection URL local
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);

// Database Name
const dbName = 'ArchiDati';
const collectionName= 'collection1';

// Constants
const PORT = 8443;
const HOST = 'localhost';

const dirName= '\Users\andre\Documents\WebApplication\Node\node-mongo';
var path = require('path');



// HELLO WORLD
const app = express();
app.get('/', (req, res) => {
  res.status(200).sendFile(path.resolve("view/index.html"));
});




// fidAll
app.get('/findAll', (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(dbName);
    var query = { status: true };
    dbo.collection(collectionName).find().toArray(function(err, result) {
      if (err) throw err;
      console.log(result);
      res.send(result);
      db.close();
    });
  });
  });


app.get('/findOne', (req,res) =>{
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(dbName);
    var query = { status: true };
    dbo.collection(collectionName).find(query).toArray(function(err, result) {
      if (err) throw err;
      console.log(result);
      res.send(result);
      db.close();
    });
  });
});


app.post('/create' , async (req , res) => {
  console.log(req.body);
  //const documents = req.body;
  const documents=req.query.documents;
  console.log(documents);
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(dbName);
    var query = { status: true };
    dbo.collection(collectionName).insertMany( [  documents  ]);
    res.send("created");
  });
});

// Connect to Mongo on start
// client.connect()
// const db = client.db(dbName);

//express host and port expose
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`)
